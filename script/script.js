function twitterPage(url) {
    return fetch(url)
        .then(response => {
            if(!response) {
                throw new Error("Error 1");
            }
            return response.json();
        });
}

const url1 = "https://ajax.test-danit.com/api/json/users";
const url2 = "https://ajax.test-danit.com/api/json/posts";

Promise.all([twitterPage(url1), twitterPage(url2)])
    .then(response => {
        const [data1, data2] = response;

        console.log("Data 1: ", data1);
        console.log("Data 2: ", data2);

        data1.forEach(user => {
            data2.forEach(post => {
                if (user.id === post.userId) {
                    const card = new Card(user.username, user.email, user.id, post.title, post.body);
                    card.createCard();
                }
            });
        });
    })
    .catch(error => {
        console.error("Error 2", error);
    });

class Card {
    constructor(username, email, id, title, body) {
        this.username = username;
        this.email = email;
        this.id = id;
        this.title = title;
        this.body = body;
    }

    createCard() {
        const cardContainer = document.getElementById("posts-container");

        const cardDiv = document.createElement("div");
        cardDiv.classList.add("card");
        cardDiv.dataset.userId = this.id;
        cardDiv.style.background = "#6aabbd";
        cardDiv.style.margin = "20px";
        cardDiv.style.padding = "20px";
        cardDiv.style.borderRadius = "10px";

        const cardUsername = document.createElement("p");
        cardUsername.textContent = this.username;
        cardUsername.style.fontSize = "26px";
        cardUsername.style.marginTop = "0";
        cardUsername.style.marginBottom = "-15px";

        const cardEmail = document.createElement("p");
        cardEmail.textContent = this.email;
        cardEmail.style.fontSize = "13px";
        cardEmail.style.color = "#806668";

        const cardTitle = document.createElement("p");
        cardTitle.textContent = this.title;
        cardTitle.style.fontSize = "22px";

        const cardBody = document.createElement("p");
        cardBody.textContent = this.body;

        const cardDelete = document.createElement("button");
        cardDelete.textContent = "Delete";
        cardDelete.classList.add("delete-button");
        cardDelete.style.borderRadius = "10px";
        cardDelete.style.background = "#806668";
        cardDelete.style.color = "#d4d4d4";
        cardDelete.style.fontSize = "16px";
        cardDelete.userId = this.id;
        cardDelete.addEventListener("click", () => this.deleteCard(this.id, cardDiv));

        cardContainer.appendChild(cardDiv);
        cardDiv.appendChild(cardUsername);
        cardDiv.appendChild(cardEmail);
        cardDiv.appendChild(cardTitle);
        cardDiv.appendChild(cardBody);
        cardDiv.appendChild(cardDelete);
    }

    deleteCard(userId, cardDiv) {
        const deleteUrl = `https://ajax.test-danit.com/api/json/posts/${userId}`;

        if(confirm("remove this post?")) {
            fetch(deleteUrl, {
                method: "DELETE"
            })
            .then(response => {
                if (!response.ok) {
                    throw new Error("Error 3");
                }
            })
            .then(() => {
                cardDiv.remove();
            })
            .catch(error => {
                console.error("Error 4", error);
            });
        }
    }
}

